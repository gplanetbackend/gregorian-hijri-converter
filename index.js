/**
 * Created by khaled on 26/12/18.
 */
const moment = require('moment-hijri');
const express = require('express');
const app = express();
const port = 3000;

app.listen(port, () => console.log(`App listening on port ${port}!`));

app.get('/api/hijri-to-gregorian/:hijri', (req, res) => {
    moment.locale('en');
    $gregorian = moment(req.params.hijri, 'iYYYY-iM-iD').format('D-M-YYYY');
    return res.status(200).json({date: $gregorian});
});
app.get('/api/gregorian-to-hijri/:gregorian', (req, res) => {
    moment.locale('ar');
    $hijri = moment(req.params.gregorian, 'D-M-YYYY').format('iYYYY/iM/iD');
    return res.status(200).json({date: $hijri});
});